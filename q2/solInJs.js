// Mocking WPDB
const wpdb = {
  prepare: function (query, item1, item2) {
    console.log(query, item1, item2);
    return true;
  },
  get_results: async function (success) {
    if (!success) return [];
    return [{ column_name: "First" }];
  },
};

// Mocking wp_remote_get
async function wp_remote_get() {
  return true;
}

// Example data sets
const data_set_1 = ["A", "B", "C", "D", "E"];
const data_set_2 = ["1A", "2B", "3C", "4D", "5E"];

async function processDoubleCols(col1, col2) {
  const query =
    "SELECT * FROM wp_example_table WHERE column1 = %s AND column2 = %s";
  const result = await wpdb.get_results(wpdb.prepare(query, col1, col2));

  // Make web service call
  const api_response = await wp_remote_get(
    `https://example.com/api/service?param1=${col1}&param2=${col2}`,
  );

  // Process and display the results
  if (api_response !== null) {
    console.log(`API Response for ${col1} and ${col2}: ${api_response}`);
  }
  result?.forEach?.((row) => {
    console.log(`Processing row: ${row.column_name}`);
  });
}

// Finding matching pairs to reduce the double loop from O(n^2) to O(N * Log(N))
const matchedPairs = data_set_1
  .map((item1) => {
    const match = data_set_2.find((item2) => item2.indexOf(item1) >= 0);
    if (!match) return null;
    return [item1, match];
  })
  .filter(Boolean);

// Process all matching pairs at the same time in async
Promise.all(matchedPairs.map((pair) => processDoubleCols(pair[0], pair[1])));
