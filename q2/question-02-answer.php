<?php
global $wpdb;
use React\Promise\Promise;
use React\Promise\PromiseInterface;

// Example data sets
$data_set_1 = array('A', 'B', 'C', 'D', 'E');
$data_set_2 = array('1A', '2B', '3C', '4D', '5E');

function processDoubleCols($col1, $col2) {

    $query = "SELECT * FROM wp_example_table WHERE column1 = %s AND column2 = %s"; 
    $result = $wpdb->get_results($wpdb->prepare($query, $col1, $col2));

    // Make web service call
		$api_response = wp_remote_get('https://example.com/api/service?param1=' . $col1 . '&param2=' . $col2);

    // Process and display the results
    if ($api_response !== null) {
       echo "API Response for $item1 and $item2: $api_response"; 
    }

    foreach ($result as $row) {
       echo "Processing row: " . $row->column_name; 
    }
}

// Loop the two arrays but break if finding match to avoid complexity, reduction expected from O(n^2) to O(N*Log(N))
$matchedPairs = [];
foreach ($data_set_1 as $item1) {
    $matchFound = false;
    foreach ($data_set_2 as $item2) {
        if (strpos($item2, $item1) !== false) {
            $matchedPairs[] = [$item1, $item2];
            $matchFound = true;
            break;
        }
    }

    // Only add the pair if a match was found
    if (!$matchFound) {
        $matchedPairs[] = null;
    }
}

// Filter out null values
$filteredPairs = array_filter($matchedPairs);

// Turning the processing of these matching pairs to run async so they don't wait for each other to process

// Map pairs to promises
$promises = array_map(function ($pair) {
    return processDoubleCols($pair[0], $pair[1]);
}, $filteredPairs);

// Resolve all promises concurrently
React\Promise\all($promises)->then(function ($results) {
    // All promises resolved successfully
    foreach ($results as $result) {
        echo $result . PHP_EOL;
    }
}, function ($error) {
    // Handle errors if any promise fails
    echo "Error: " . $error->getMessage() . PHP_EOL;
});

// Start the event loop
$loop = React\EventLoop\Factory::create();
$loop->run();
?>
