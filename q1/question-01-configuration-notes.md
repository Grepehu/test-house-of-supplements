## Requirements

- The message bus should support any number of producers (via REST API): Using **Amazon API Gateway** we can create, publish and manitain as many APIs as needed inside our infrastructure so this shouldn't be a problem.
- The message bus should support different message types (ie ‘Orders’, ‘Products’ or ‘Transactions’): Using **Amazon SQS** we can deploy multiple micro-services and have a separate queue for each of them, so each type of message can have their own queue. We can alsop couple this with **Amazon SNS** to set up subcription calls that send out messages after the message type is initially handled (in a push notification style) to ensure the messages are based on the message type.
- The message bus must ensure at most once delivery and guarantee processing order: on **Amazon SQS** we can enable "Deduplication" to ensure at most once delivery and set up "Visibility Timeout" to prevent consumer from processing again the same messages.
- The message bus should support three different ‘consumers’, and a separate queue for each message type: Again this can both be handled with **Amazon SQS** and **Amazon SNS**.
- Your design should include suitable resources to handle message failure and retries, indicate which configuration options should be utilised to achieve this: The API Gateway could be pointing to **Amazon Lambda** functions in which we could properly handle errors and retries.

## Solutions

So in general we would need:

- **Amazon SQS**: To manage the queues for different message types ('Orders', 'Products', 'Transactions'). Each consumer will have its own queue.
- **Amazon SNS**: As a publish and subscribe service, it can distribute messages to multiple subscribers integrating with SQS to send out messages to multiple queues based on message type.
- **AWS Lambda**: For processing messages. Lambda functions can be triggered by SQS events and can process messages according to their type.
- **API Gateway**: To expose REST APIs to send messages.

## Configuration

Some configuration notes:

- **SQS Queues**: Enable "Deduplication" to ensure at most once delivery. Configure "Visibility Timeout" to prevent consumers from reprocessing messages.
- **SNS Topic**: Set up subscriptions to SQS queues for different message types. Ensure "Content-Based Routing" is enabled to route messages to the correct queue based on the message type.
- **Lambda Functions**: Implement error handling within the function to retry failed operations. Use "Dead Letter Queues" (DLQs) for messages that cannot be processed successfully after retries.
- **API Gateway**: Secure the REST API using IAM roles and policies. Configure rate limiting to prevent abuse.
